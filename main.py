
def print_hello_world():

    print("hello! this is devops assignemnt 1")


print_hello_world()

#First function
print("What is your name?")
name = input()

def printName():

    print("Hello " + name + "!")

printName()

### Function that tells you the weather after you enter a city name
import requests
import json

apiKey = "f4132a15866020820e96f47dcd5ed04b"

site_url = "http://api.openweathermap.org/data/2.5/weather?"

city = input("Enter city name : ")

url = site_url + "appid=" + apiKey + "&units=metric" + "&q=" + city

response = requests.get(url)

x = response.json()

if x["cod"] != "404":

    y = x["main"]

    temperature = y["temp"]
    feelsLike = y["feels_like"]

    z = x["weather"]

    description = z[0]["description"]

    print(" Temperature (in Celcius) = " +
          str(temperature) +
          "\n feels like = " +
          str(feelsLike) +
          "\n description = " +
          str(description))
else:
    print("City Not Found")
